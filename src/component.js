import Vue from 'vue';


Vue.component('home', require('./components/Home').default);
Vue.component('matches', require('./components/Matches').default);
Vue.component('inning-score', require('./components/InningScore').default);

