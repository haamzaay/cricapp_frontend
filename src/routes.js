import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

let paths = (require('./routes/path.js')).default;
console.log(paths);
export default new VueRouter({
    routes : paths
});