import Vue from 'vue';

export default [ 
    
  {
   path: '/', component: Vue.component('home'),
  },
  {
    path: '/matches/:id', name: 'matches', component: Vue.component('matches'), props: true,
  },
]