import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)
axios.defaults.baseURL = '/api'
export default new Vuex.Store({
    state: {

    },
    getters: {
        
    },
    mutations: {
        
    },
    actions: {

    }
})